RESUME
------
### Rajasimman S
*+91 86828 32883 | srajasimman@gmail.com*<br>
*B/4, S.P. Garden,
T. Nagar, Near Kannammapet,
Chennai - 600017.*

---
#### Profile:

I am an enthusiastic, hands-on IT professional with 8 years experience in support (From 1st to 3rd line). I have a proven track record of managing and maintaining Windows / Linux servers and desktops in LAN/WAN network infrastructures, and adopt a meticulous approach to ensure the smooth-running of business operations. Qualified to industry standards, I enjoy closely following the latest changes in technology and apply this to my duties wherever relevant.

---
#### Skills Summary:

- Windows, Mac iOS, Ubuntu and Many UNIX/Linux OS Distros
- Windows 2008/2012, Ubuntu, Redhat, Fedora Server
- Openstack and CloudStack
- Amazon Web Services - EC2, S3, EBS, RDS, VPC and CloudFront
- Apache, Nginx, HA Proxy, MySQL, MongoDB, SVN, Git, Postfix and Squid Servers
- Bash, Python, AutoIT Script, Windows PowerShell and Batch Script
- Nagios and Zabbix
- VMWare ESXi, Xen, KVM and VirtualBox
- Active Directory, MS Exchange, IBM Lotus Domino Server and Linux Mail Servers
- MS Office 2003/2007/2010, Office 365 and Open Office
- MS Outlook, IBM Lotus Notes and Other Email Clients
- Networking, DNS, DHCP, LAN, WLAN and VPN
- SCCM, CA-ITCM and LANDesk
- Norton, McAfee and other Antivirus Security Software
- Web Programming, HTML and PHP
- Adobe Photoshop, Adobe Illustrator and CorelDraw

---
#### Education:

|Course|Insitution|Year|Marks %|
|:---:|---|:---:|:---:|
|S.S.L.C|C.D. Nayagam, High Sec School, Chennai.|2002|46%|
|H.S.C|C.D. Nayagam, High Sec School, Chennai.|2004|47%|
|B.C.A|University of Madras, Chennai.|2008|56%|

---
#### Technical Qualifications:

- Diploma in Computer Applications - C.S.C., Chennai.
- Diploma in Hardware and Networking - C.S.C., Chennai.
- Ubuntu Certified Professional - Canonical Online.

---
#### Employment History:
<br>__Corent Technology Pvt Ltd. Chennai__<br>
**_Systen Admin IT/Cloud - Support_**<br>
*Oct 2015 - Till Date*

- Administration of Cloud Infrastructures in Amazon Web Services and Microsoft Azure Cloud
- Administration of In house Data Centre Infrastructures and Private Cloud
- Create, configure, monitor and management of Private Cloud Infrastructures
- Application Deployment, Configure, Backup/Restore and Upgrade
- Design and Development of Backup and Deployment Scripts
- Infrastructure Migration on Cloud Environment
- Develop Configuration Management Solutions
- Create tools to help teams make the most out of the available infrastructure
- Apache, Nginx and Tomcat webserver installations and Configuration
- MySQL, PostgreSQL and MongoDB Administration
- Cloud knowledgebase Update for Products

<br>__BRANDIDEA Consultancy P. Ltd. Chennai__<br>
**_Sr. Executive IT - Support_**<br>
*Oct 2014 - July 2016*

- Management of Cloud Infrastructures in AWS and Google Cloud
- Create, configure, monitor and management of Cloud Infrastructures
- Upload and update database and other data to cloud server
- Help tune performance and ensure high availability of infrastructure
- Responsible for supporting: Windows XP/Vista/Windows 7/10, Linux Desktop, Office Suite, Office 365, Windows/ Linux Server, Active Directory management, MS Exchange 2003/2007, Lotus Domino Server, iPhone/Android/Windows Mobile, Backup products, Anti-Virus products, Bio Metric Devices, CCTV Systems, DNS/DHCP, TCP/IP, Ethernet, wireless router and Firewall Configurations.
- Diagnosis of desktop, application, networking and infrastructure issues.
- Administering the IT department's policies and procedures.
- Maintaining a log of all problems detected and system backups.
- Responsible for maintaining backups and crash recovery.
- Working closely with software suppliers to resolve operational issues.

<br>__Hella India Automotive Private Limited. Delhi.__<br>
*(Location: Hella India Automotive Lighting Technical Centre. Chennai)*<br>
**_IT Engineer - Infrastructure Management_**<br>
*Sept 2012 - Sept 2014*

- Management of Windows/Linux clients and servers
- First and Second level support for network related issues
- Installation of operating system and application on new systems
- Update operating system and application patch on client systems
- Installation of CAD applications (CATIA and Autodesk Moldflow)
- Deploy Software package via CA IT Client Management
- Configure, Monitor and management of network data storages
- Configure the Lotus Notes email client for users
- Support users via remote assistance tools
- IT Asset management and IT Peripherals stock management

<br>__CMS Infosystems Private Limited. Chennai.__<br>
*(Client: Ashok Leyland's Technical Centre.)*<br>
**_IT Support Engineer - L2_**<br>
*Jan 2010 - Aug 2012*

- Troubleshooting and configuring Windows XP/7 and Linux Clients for LAN Configuration of DNS, DHCP, creating user account and providing policies like user account quota, authentication
- Performed Remote Administration and remote Assistance for the users
- Software Application Deployment to Client Systems
- Taking backup periodically and restoration. Allocating the disk quota for the user
- Monitoring Performance alerts triggered by the server & Resolving Hardware & Software related calls
- Troubleshooting & installations of applications.
- Managing storage server and folder create/access control
- Provide Remote support using windows remote assistance tools.
- Troubleshooting breakdown in hardware calls and operating systems calls
- Installation, configure and troubleshoot operating systems. (NT/2000/XP and Windows 7)
- Installation of printers, scanners and peripherals & resolving the hardware issues
- Email Configuration MS Outlook, Data card and VPN
- Asset Management, Movement Tracking and Maintenance of the Database

<br>__Nanosoft Systems. Chennai.__<br>
*(Client: First Source Solutions Pvt Ltd)*<br>
**_IT Support Engineer - L1_**<br>
*Mar 2008 to Jan 2010*

- First level support for hardware/OS related calls
- Provide Remote support using windows remote assistance tools.
- User mail/Login id unlock and password reset
- Managing storage server and folder create/access control and allocating the disk quota for the users
- Installing, configuring and troubleshooting operating systems. (NT/2000/XP and Windows 7)
- Installing printers, scanners and peripherals & resolving the hardware issues
- Installation of applications and troubleshooting the errors
- Configuring MS Outlook mail client
- Deploying Software using package manager
- Taking backup periodically and restoration
- Troubleshooting & installations of applications

<br>__Nexbase Systems. Chennai.__<br>
*(Client: 300+ Clients in Chennai)*<br>
**_IT Service Engineer - Part Time_**<br>
*Apr 2006 to Dec 2007*<br>

- Assemble desktop and Installation of operating system
- Installation and Troubleshooting of Windows NT/2000/XP
- Configuring broadband/Dial-up connections
- Configuring Wired and WiFi Routers
- Configuring Outlook Express with ISP settings
- Installation of printers, scanners and peripherals & Resolving the hardware issues
- Maintenance of Wired and Wireless Networks

---
#### Personal Details:

- Name : S. Rajasimman
- Father's Name : D. Shanmugam
- Date of Birth : 21st April 1987
- Sex : Male
- Marital Status : Married
- Hobbies : Reading Historical and Fiction Novels, Watching Movies, Internet Browsing and Reading Blogs about Information Technology
- Permanent Address : B/4, S.P. Garden, Kannammapet, T.Nagar, Chennai - 600 017.
- E-mail : srajasimman@gmail.com

---
#### Declaration
*I hereby declare that all the above details furnished by me are true to the best of my knowledge. I assure I will put in the best of my efforts and strive hard for the benefit of the organization.*

Date:
<br>Place: *Chennai*


<br>(*S. Rajasimman*)
